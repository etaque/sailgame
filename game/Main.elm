module ShiftMaster where

import Window
import Keyboard
import WebSocket
import Json

import Inputs
import Game
import Steps
import Render.All as R

-- ports can't expand type alias for the moment... ugly manual expansion...
port raceInput : Signal { now: Float, startTime: Float, 
                          course: Maybe { upwind: { y: Float, width: Float }, downwind: { y: Float, width: Float }, laps: Int, 
                                          markRadius: Float, islands: [{ location : (Float,Float), radius : Float }], 
                                          bounds: ((Float,Float),(Float,Float)) },
                          opponents: [{ position : (Float,Float), direction: Float, velocity: Float, passedGates: [Float] }],
                          leaderboard: [String] }

clock : Signal Float
clock = inSeconds <~ fps 30

input : Signal Inputs.Input
input = sampleOn clock (lift6 Inputs.Input clock Inputs.chrono Inputs.keyboardInput 
                              Inputs.mouseInput Window.dimensions raceInput)

gameState : Signal Game.GameState
gameState = foldp Steps.stepGame Game.defaultGame input

port raceOutput : Signal { position : (Float, Float), direction: Float, velocity: Float, passedGates: [Float] }
port raceOutput = lift (boatToRaceOutput . .boat) gameState

boatToRaceOutput ({position, direction, velocity, passedGates} as boat) =
  { position = position, direction = direction, velocity = velocity, passedGates = passedGates }  

main = lift2 R.renderAll Window.dimensions gameState
